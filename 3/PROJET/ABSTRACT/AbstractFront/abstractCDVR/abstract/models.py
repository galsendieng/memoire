# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django import forms # Pour utiliser les élements de formulaire
from django.utils import timezone
from django.utils.translation import gettext as _
from .extra import *
# sudo pip install django-multiselectfield
from multiselectfield import MultiSelectField

# Create your models here.
"""
def __unicode__(self):
    return u'{}'.format(self.id)
"""

""" 
    PARTIE ABSTRACT MODELES
"""

class Request(models.Model):
    nom = models.CharField(max_length=255)
    link = models.URLField()
    datareq = models.TextField()
    header = models.CharField(max_length=255, default="{'Content-Type': 'application/xml'}")
    date = models.DateTimeField(auto_now=False,
                               verbose_name="Date de creation", default= timezone.now)

    def __str__(self):
        return self.nom

    class Meta:
        verbose_name = 'Requête API VOD'
        verbose_name_plural =  'Requêtes API VOD'

class Response(models.Model):
    nom = models.CharField(max_length=255)
    daterequest = models.DateTimeField(auto_now=False,
                               verbose_name="Date de test", default= timezone.now)
    link = models.URLField()
    datareq = models.TextField()
    status = models.CharField(max_length=10)
    reason = models.CharField(max_length=255)
    methode = models.CharField(max_length=100)
    body = models.TextField()
    header = models.CharField(max_length=255)
    duree = models.CharField(max_length=255)
    typetest = models.CharField(max_length=6, default='auto')
    #fichier = models.FileField(upload_to='uploads/', default='kkg.txt')

    def __str__(self):
        return self.nom

    class Meta:
        verbose_name = 'Les réponses des requêtes API VOD'
        verbose_name_plural = 'Les réponses des requêtes API VOD'

class ListeDeMode(models.Model):
    name = models.CharField(max_length=128)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Liste des Modes de Streaming'
        verbose_name_plural =  'Listes des Modes de Streaming'

class Mode(models.Model):
    objet = models.ForeignKey(ListeDeMode)
    nameMode = models.CharField(max_length=100)
    staticAbr = models.CharField(max_length=100)
    fragment = models.IntegerField(default=2)
    manifestSuffix = models.CharField(max_length=10)
    deviceProfil = models.CharField(max_length=10)
    client = models.CharField(max_length=100, default='null')

    def __str__(self):
        return self.nameMode

    class Meta:
        verbose_name = 'Mode de Streaming'
        verbose_name_plural = 'Mode de Streaming'

class NamePod(models.Model):
    name = models.CharField(max_length=100, unique=True)
    link = models.CharField(max_length=100,default='strm.podX.manager.cdvr.orange.fr', unique=True)
    description = models.TextField(null=True, default='Description du Pod')
    created = models.DateTimeField(auto_now=False,
                               verbose_name="Date de creation", default= timezone.now)

    def __str__(self):
        return str(self.name)

    class Meta:
        verbose_name = 'Nom des Pods du VSPP'
        verbose_name_plural ='Noms des Pods du VSPP'

class ListeDePod(models.Model):
    name = models.ForeignKey(NamePod, blank=True, null=True)
    created = models.DateTimeField(auto_now=False,
                               verbose_name="Date de creation", default= timezone.now)
    def __str__(self):
        return str(self.name)

    class Meta:
        verbose_name = 'Liste des Pods'
        verbose_name_plural ='Listes des Pods'

class NameChaine(models.Model):
    name = models.CharField(max_length=100)
    description = models.TextField(default='Description de la Chaine')
    created = models.DateTimeField(auto_now=False,
                               verbose_name="Date de creation", default= timezone.now)

    def __str__(self):
        return str(self.name)

    class Meta:
        verbose_name = 'Nom de la Chaine'
        verbose_name_plural = 'Noms des Chaines'

class Chaine(models.Model):
    objet = models.ForeignKey(ListeDePod)
    name = models.ForeignKey(NameChaine)

    def __str__(self):
        return str(self.name)

    class Meta:
        verbose_name = 'La Chaine'
        verbose_name_plural = 'Les Chaines du VSPP'

class NameNode(models.Model):
    name = models.CharField(max_length=100, unique=True)
    description = models.TextField(null=True, default='Description du Noeud')
    created = models.DateTimeField(auto_now=False,
                               verbose_name="Date de creation", default= timezone.now)

    def __str__(self):
        return str(self.name)

    class Meta:
        verbose_name = 'Les Noeud du VSPP '
        verbose_name_plural ='Les Noeuds du VSPP '

class Streamer(models.Model):
    objet = models.ForeignKey(ListeDePod, blank=True, null=True)
    node = models.ForeignKey(NameNode, blank=True, null=True)
    address = models.GenericIPAddressField(protocol="ipv4", unpack_ipv4=False)
    created = models.DateTimeField(auto_now=False,
                               verbose_name="Date de creation", default= timezone.now)

    def __str__(self):
        return str(self.address)

COMMANDES = (('live', 'LIVE'),
              ('catchup', 'CATCHUP'),
              ('startover', 'STARTOVER'),
              ('vodplayout', 'VOD PLAYOUT'),
              ('vodprepackaged', 'VOD PREPACKAGED'),
              ('ntc', 'NTC'),
              ('npvr', 'nPVR')
             )
MODES = (
    ( "['smooth': { 'staticabr': 'shss', 'fragment': '2', 'manifestsuffix': 'ism', device': 'SMOOTH_2S',client: null }]", _('SMOOTH')),
    ( "['dash': { 'staticabr': 'sdash', 'fragment': '2', 'manifestsuffix': 'mpd', device': 'DASH_2S',client: null }]", _('DASH')),
    ( "['hls': { 'staticabr': 'shls', 'fragment': '10', 'manifestsuffix': 'm3u8', device': 'HLS_LOW',client: null }]", _('HLS')),
)

class VSPP(models.Model):
    name = models.CharField(max_length=250, default='MISTRAL', unique = True)
    description = models.TextField()
    date = models.DateTimeField(auto_now=False,
                               verbose_name="Date de creation", default= timezone.now)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'VSPP '
        verbose_name_plural = 'Les VSPPs de Tests '


class Configuration(models.Model):
    HTTP = 'http'
    HTTPS = 'https'
    #FTP = 'ftp'
    PROTOCOLE_CHOICES = (
        (HTTP, 'http'),
        (HTTPS, 'https'),
       # (FTP, 'ftp'),
    )
    nameConfig = models.CharField(max_length=100)
    vspp = models.ForeignKey(VSPP, on_delete=models.CASCADE, default=1)
    protocole = models.CharField(
        max_length=5,
        choices=PROTOCOLE_CHOICES,
        default=HTTP,
    )
    port = models.PositiveIntegerField(default=5555)
    intervalle = models.PositiveIntegerField(default=10)
    emailFrom = models.EmailField(default='adama.dieng@orange.com', editable=False)
    smsFrom = models.CharField(max_length=13, default='+33768225617', editable=False)
    schemasmooth = models.FileField(upload_to="schemas/smooth/%Y/%m/%d", validators=[validate_file_extension])
    schemadash=  models.FileField(upload_to="schemas/dash/%Y/%m/%d", validators=[validate_file_extension])
    nameOfLevelFile= models.CharField(max_length=50, default='Level')
    #folderOfLevelFile= models.FilePathField(path="/home/adama/images", match="foo.*", recursive=True)
    folderOfLevelFile = models.CharField(max_length=150, editable=False,
                                         default='/home/adama/PROJET/ABSTRACT/AbstractBackend/Levels/')
    nameOfIframeFile= models.CharField(max_length=50, default='Iframe')
    folderOfIframeFile = models.CharField(max_length=150,editable=False,
                                          default='/home/adama/PROJET/ABSTRACT/AbstractBackend/Iframe/')
    livefoldersmooth = models.CharField(max_length=150,editable=False,
                                        default='/home/adama/PROJET/ABSTRACT/AbstractBackend/manifests/dynamic/MSS/Live/')
    catchupfoldersmooth = models.CharField(max_length=150,editable=False,
                                           default='/home/adama/PROJET/ABSTRACT/AbstractBackend/manifests/dynamic/MSS/Catchup/')
    livefolderdash = models.CharField(max_length=150,editable=False,
                                      default='/home/adama/PROJET/ABSTRACT/AbstractBackend/manifests/dynamic/DASH/Live/')
    catchupfolderdash =  models.CharField(max_length=150,editable=False,
                                      default='/home/adama/PROJET/ABSTRACT/AbstractBackend/manifests/dynamic/DASH/Catchup/')
    livefolderhls = models.CharField(max_length=150,editable=False,
                                     default='/home/adama/PROJET/ABSTRACT/AbstractBackend/manifests/dynamic/HLS/Live/')
    catchupfolderhls = models.CharField(max_length=150,editable=False,
                                        default='/home/adama/PROJET/ABSTRACT/AbstractBackend/manifests/dynamic/HLS/Catchup/')
    logdirectory = models.CharField(max_length=150,editable=False,
                                    default='/home/adama/PROJET/ABSTRACT/AbstractBackend/logabstract')
    logprefixname = models.CharField(max_length=50, default='abstractLog')
    created = models.DateTimeField(auto_now=False,
                               verbose_name="Date de creation",
                                   default= timezone.now)
    pods = models.ManyToManyField(ListeDePod)
    #modes = models.ManyToManyField(ListeDeMode)
    commandesATraiter = MultiSelectField(choices=COMMANDES, min_choices=1, default='live',)
    modesATraiter = MultiSelectField(choices=MODES, min_choices=3, )
    def __str__(self):
        return str(self.nameConfig)

CATEGORY_CHOICES = (
    (1, _('Handbooks and manuals by discipline')),
    (2, _('Business books')),
    (3, _('Books of literary criticism')),
    (4, _('Books about literary theory')),
    (5, _('Books about literature')),
)

TAGS_CHOICES = (
    ( "['smooth': { 'staticabr': 'shss', 'fragment': '2', 'manifestsuffix': 'ism', device': 'SMOOTH_2S',client: null }]", _('SMOOTH')),
    ( "['dash': { 'staticabr': 'sdash', 'fragment': '2', 'manifestsuffix': 'mpd', device': 'DASH_2S',client: null }]", _('DASH')),
    ( "['hls': { 'staticabr': 'shls', 'fragment': '10', 'manifestsuffix': 'm3u8', device': 'HLS_LOW',client: null }]", _('HLS')),
)

PROVINCES = (
    ('AB', _("Alberta")),
    ('BC', _("British Columbia")),
)

STATES = (
    ('AK', _("Alaska")),
    ('AL', _("Alabama")),
    ('AZ', _("Arizona")),
)

PROVINCES_AND_STATES = (
    (_("Canada - Provinces"), PROVINCES),
    (_("USA - States"),       STATES),  # noqa: E241
)

class Book(models.Model):
    title = models.CharField(max_length=200)
    categories = MultiSelectField(choices=CATEGORY_CHOICES,
                                  max_choices=3,
                                  # default='1,5')
                                  default=1)
    tags = MultiSelectField(choices=TAGS_CHOICES,
                            null=True, blank=True)
    published_in = MultiSelectField(_("Province or State"),
                                    choices=PROVINCES_AND_STATES,
                                    max_choices=2)

    def __str__(self):
        return self.title

    def __unicode__(self):
        return self.__str__()

class Document(models.Model):
    file = models.FileField(upload_to="documents/%Y/%m/%d", validators=[validate_file_extension])
