#! /usr/bin/python
# -*- coding:utf-8 -*-

from flask import Flask, render_template
from flaskext.mysql import MySQL
mysql = MySQL()
app = Flask(__name__, static_folder="", static_url_path='')
app.config['SECRET_KEY'] = 'F34TF$($e34D';
app.config['MYSQL_DATABASE_USER'] = 'adama'
app.config['MYSQL_DATABASE_PASSWORD'] = "adama"
app.config['MYSQL_DATABASE_DB'] = 'training'
app.config['MYSQL_DATABASE_HOST'] = '192.168.134.122'

from datetime import date
app = Flask(__name__)
mysql.init_app(app)


@app.route('/date')
def date():
    d = date.today().isoformat()
    return render_template('date.html', la_date=d)

@app.route('/view')
def accueil():
    mots = ["bonjour", "a", "toi,", "visiteur."]
    return render_template('accueil.html', titre="Bienvenue !", mots=mots)

@app.route('/')
def liste():
    import MySQLdb
    conn = MySQLdb.connect(db='training', user='adama', passwd='adama', host='192.168.134.122')
    cursor = conn.cursor()
    cursor.execute("SELECT * from responses ORDER BY id desc")
    data = cursor.fetchall()
    return render_template('test.html',  data=data)

@app.errorhandler(401)
@app.errorhandler(404)
@app.errorhandler(500)
def ma_page_erreur(error):
    error = error.code
    return render_template('404.html',error = error)

if __name__ == '__main__':
    app.run(debug=True)
    app.run("0.0.0.0")
