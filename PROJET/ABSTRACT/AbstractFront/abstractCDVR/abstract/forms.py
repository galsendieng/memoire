# -*- coding: ISO-8859-1 -*-
from django import forms
"""
from multi_email_field.forms import MultiEmailField

class SendMessageForm(forms.Form):
    emails = MultiEmailField()

"""
from django import forms


class ContactForm(forms.Form):

    sujet = forms.CharField(max_length=100)

    message = forms.CharField(widget=forms.Textarea)

    envoyeur = forms.EmailField(label="Votre adresse mail")

    renvoi = forms.BooleanField(help_text="Cochez si vous souhaitez obtenir une copie du mail envoy�.", required=False)