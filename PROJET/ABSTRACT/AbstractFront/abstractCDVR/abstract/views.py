# -*- coding: utf-8 -*-
from django.http import HttpResponse
__author__ = 'adama'
from django.shortcuts import render
from .forms import ContactForm
from django.shortcuts import render_to_response
from django.contrib.auth.decorators import login_required
from django.template.context import RequestContext

@login_required
def home(request):
    """ Exemple de page HTML, non valide pour que l'exemple soit concis """
    #return HttpResponse(text)
    return render(request, 'abstract/home.html')
