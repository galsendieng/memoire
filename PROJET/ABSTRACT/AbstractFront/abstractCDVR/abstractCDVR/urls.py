#-*- coding: utf-8 -*-
from django.conf.urls import url, include
from django.contrib import admin

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^', include('abstract.urls')),
    #url('^inbox/notifications/', include(notifications.urls, namespace='notifications')),
    url(r'^email_confirmation/', include('email_confirm_la.urls', namespace='email_confirm_la')),
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
]
# Change admin site title
#admin.site.site_header = _("ABSTRACT")
#admin.site.site_title = _("ABSTRACT")
