# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.utils import timezone

# Create your models here.

""" PARTIE ABSTRACT """

class Pod(models.Model):
    name = models.CharField(max_length=100)
    link = models.CharField(max_length=100)
    description = models.TextField(null=True, default='NULL')
    created = models.DateField()

    def __str__(self):
        # __unicode__ on Python 2
        return "%s %s" % (self.name, self.link)

class Streamer(models.Model):
    nomstreamer = models.CharField(max_length=100)
    address = models.GenericIPAddressField()
    created = models.DateField()
    pods = models.ForeignKey(Pod, on_delete=models.CASCADE)
    description = models.TextField(null=True, default='Description du streamer')

    def __str__(self):
        # __unicode__ on Python 2
        return self.nomstreamer

    class Meta:
        ordering = ('nomstreamer',)

class Channel(models.Model):
    nom = models.CharField(max_length=100)
    description = models.TextField(null=True, default='NULL')
    created = models.DateField()
    pods = models.ForeignKey(Pod, on_delete=models.CASCADE)

    def __str__(self):
        # __unicode__ on Python 2
        return self.nom

    class Meta:
        ordering = ('nom',)

""" Un mode peut contenir 1 ou plusieurs types (Smooth | Dash | Hls ) """
class Mode(models.Model):
    nameMode = models.CharField(max_length=100)
    created = models.DateField()
    description = models.TextField(null=True, default='Description du mode ( SMOOTH | DASH | HLS )')
    def __str__(self):              # __unicode__ on Python 2
        return self.nameMode

    class Meta:
        ordering = ('nameMode',)

class TypeMode(models.Model):
    name = models.CharField(max_length=100)
    staticAbr = models.CharField(max_length=100)
    fragment = models.IntegerField(default=2)
    manifestSuffix = models.CharField(max_length=10)
    deviceProfil = models.CharField(max_length=10)
    client = models.CharField(max_length=100, default='null')
    description = models.TextField(null=True, default='Type de mode')
    created = models.DateField()
    mode = models.ManyToManyField(Mode)

    def __str__(self):              # __unicode__ on Python 2
        return self.name

    class Meta:
        ordering = ('name',)


class Commande(models.Model):
    nameCommande = models.CharField(max_length=100)
    created = models.DateField()
    description = models.TextField(null=True, default='Description de la Commande')
    def __str__(self):              # __unicode__ on Python 2
        return self.nameCommande

    class Meta:
        ordering = ('nameCommande',)

class TypeCommande(models.Model):
    name = models.CharField(max_length=100)
    period = models.IntegerField(default=0)
    start = models.CharField(max_length=50, default='start')
    end = models.CharField(max_length=50, default='end')
    description = models.TextField(null=True, default='Type de commande executée')
    created = models.DateField()
    commande = models.ManyToManyField(Commande)

    def __str__(self):              # __unicode__ on Python 2
        return self.name

    class Meta:
        ordering = ('name',)


class Request(models.Model):
    nom = models.CharField(max_length=255)
    link = models.URLField()
    datareq = models.TextField()
    header = models.CharField(max_length=255, default="{'Content-Type': 'application/xml'}")
    date = models.DateTimeField(auto_now=False,
                               verbose_name="Date de creation", default= timezone.now)

    def __str__(self):
        return self.nom

class Response(models.Model):
    nom = models.CharField(max_length=255)
    daterequest = models.DateTimeField(auto_now=False,
                               verbose_name="Date de test", default= timezone.now)
    link = models.URLField()
    datareq = models.TextField()
    status = models.CharField(max_length=10)
    reason = models.CharField(max_length=255)
    methode = models.CharField(max_length=100)
    body = models.TextField()
    header = models.CharField(max_length=255)
    duree = models.CharField(max_length=255)

    def __str__(self):
        return self.nom
