# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from .models import *
# Register your models here.

class StreamerAdmin(admin.ModelAdmin):
    #list_display = ('titre', 'headline', 'pub_date', 'apercu_contenu')
    list_display = ('nomstreamer', 'address', 'created')
    list_filter = ('nomstreamer', 'pods','created')
    date_hierarchy = 'created'
    ordering = ('pods','created',)
    search_fields = ('nomstreamer', 'address','pods')

    """
    def apercu_contenu(self, article):
    """
    """
        Retourne les 40 premiers caractères du contenu de l'article,
        suivi de points de suspension si le texte est plus long.

        On pourrait le coder nous même, mais Django fournit déjà la
        fonction qui le fait pour nous !
    """
    """
        return Truncator(article.contenu).chars(40, truncate='...')

    # En-tête de notre colonne
    apercu_contenu.short_description = 'Aperçu du contenu'
    """
    # Configuration du formulaire d'édition
    fieldsets = (
        # Fieldset 1 : meta-info (nomstreamer, pods…)
       ('Informations principales du streamer(*)', {
            'classes': ['collapse', ],
            'fields': ('nomstreamer', 'address', 'pods','created')
        }),
        # Fieldset 2 : description du streamer
        ('Description du streamer', {
           'description': 'Le formulaire accepte les balises HTML. Utilisez-les à bon escient !',
           'fields': ('description', )
        }),
    )

    # Colonnes personnalisées
    def apercu_contenu(self, streamer):
        """
        Retourne les 40 premiers caractères du contenu de l'article. S'il
        y a plus de 40 caractères, il faut rajouter des points de suspension.
        """
        text = streamer.description[0:40]
        if len(streamer.description) > 40:
            return '%s…' % text
        else:
            return text

    apercu_contenu.short_description = 'Aperçu de la description'


class ChannelAdmin(admin.ModelAdmin):
    #list_display = ('titre', 'headline', 'pub_date', 'apercu_contenu')
    list_display = ('nom', 'description', 'created','pods')
    list_filter = ('nom', 'description', 'created','pods')
    date_hierarchy = 'created'
    ordering = ('created',)
    search_fields = ('nom', 'pods')

    # Configuration du formulaire d'édition
    fieldsets = (
        # Fieldset 1 : meta-info (titre, auteur…)
       ('Général', {
            'classes': ['collapse', ],
            'fields': ('nom',  'created','pods')
        }),
        # Fieldset 2 : contenu de l'article
        ('Contenu de la Chaine', {
           'description': 'Le formulaire accepte les balises HTML. Utilisez-les à bon escient !',
           'fields': ('description',)
        }),
    )

class CommandeAdmin(admin.ModelAdmin):
    #list_display = ('titre', 'headline', 'pub_date', 'apercu_contenu')
    list_display = ('nameCommande', 'description', 'created')
    list_filter = ('nameCommande', 'description', 'created')
    date_hierarchy = 'created'
    ordering = ('created',)
    search_fields = ('nameCommande',)

    # Configuration du formulaire d'édition
    fieldsets = (
        # Fieldset 1 : meta-info (titre, auteur…)
       ('Général', {
            'classes': ['collapse', ],
            'fields': ('nameCommande',  'created')
        }),
        # Fieldset 2 : contenu de l'article
        ('Détails', {
           'description': 'Le formulaire accepte les balises HTML. Utilisez-les à bon escient !',
           'fields': ('description',)
        }),
    )
class ModeAdmin(admin.ModelAdmin):
    #list_display = ('titre', 'headline', 'pub_date', 'apercu_contenu')
    list_display = ('nameMode', 'description', 'created')
    list_filter = ('nameMode', 'description', 'created')
    date_hierarchy = 'created'
    ordering = ('created',)
    search_fields = ('nameMode',)

    # Configuration du formulaire d'édition
    fieldsets = (
        # Fieldset 1 : meta-info (titre, auteur…)
       ('Infos principales', {
            'classes': ['collapse', ],
            'fields': ('nameMode',  'created')
        }),
        # Fieldset 2 : contenu de l'article
        ('Détails', {
           'description': 'Le formulaire accepte les balises HTML. Utilisez-les à bon escient !',
           'fields': ('description',)
        }),
    )
class PodAdmin(admin.ModelAdmin):
    #list_display = ('titre', 'headline', 'pub_date', 'apercu_contenu')
    list_display = ('name', 'link', 'description', 'created')
    list_filter = ('name', 'link', 'created')
    date_hierarchy = 'created'
    ordering = ('created',)
    search_fields = ('name',)

    # Configuration du formulaire d'édition
    fieldsets = (
        # Fieldset 1 : meta-info (titre, auteur…)
       ('Infos principales du Pod', {
            'classes': ['collapse', ],
            'fields': ('name', 'link', 'created')
        }),
        # Fieldset 2 : contenu de l'article
        ('Description du Pod', {
           'description': 'Le formulaire accepte les balises HTML. Utilisez-les à bon escient !',
           'fields': ('description',)
        }),
    )

class TypeCommandeAdmin(admin.ModelAdmin):
    #list_display = ('titre', 'headline', 'pub_date', 'apercu_contenu')
    list_display = ('name', 'period', 'start', 'end', 'description', 'created')
    list_filter = ('name', 'period', 'created')
    date_hierarchy = 'created'
    ordering = ('created',)
    search_fields = ('name',)

    # Configuration du formulaire d'édition
    fieldsets = (
        # Fieldset 1 : meta-info (titre, auteur…)
       ('Infos principales de la Commande', {
            'classes': ['collapse', ],
            'fields': ('name', 'period', 'start', 'end',  'created')
        }),
        # Fieldset 2 : contenu de l'article
        ('Description du Pod', {
           'description': 'Le formulaire accepte les balises HTML. Utilisez-les à bon escient !',
           'fields': ('commande', 'description',)
        }),
    )

class RequestAdmin(admin.ModelAdmin):
    #list_display = ('titre', 'headline', 'pub_date', 'apercu_contenu')
    list_display = ('nom', 'link', 'date', 'datareq', 'header')
    list_filter = ('nom', 'link', 'date', 'header')
    date_hierarchy = 'date'
    ordering = ('nom',)
    search_fields = ('nom', 'link','date', )

    # Configuration du formulaire d'édition
    fieldsets = (
        # Fieldset 1 : meta-info (titre, auteur…)
       ('Général', {
            'classes': ['collapse', ],
            'fields': ('nom', 'link', 'date')
        }),
        # Fieldset 2 : contenu de l'article
        ('Description de la requête', {
           'description': 'Le formulaire accepte les balises HTML. Utilisez-les à bon escient !',
           'fields': ('datareq', 'header')
        }),
    )

class ResponseAdmin(admin.ModelAdmin):
    list_display = ('nom', 'daterequest', 'link', 'datareq', 'status', 'body')
    list_filter = ('nom', 'daterequest', 'link', 'datareq', 'status', 'reason')
    date_hierarchy = 'daterequest'
    ordering = ('daterequest', 'nom')
    search_fields = ('nom', 'daterequest', 'link', 'status', 'reason',)

    # Configuration du formulaire d'édition
    fieldsets = (
       ('Général', {
            'classes': ['collapse', ],
            'fields': ('nom', 'daterequest', 'link', 'status', 'reason', 'methode', )
        }),
        # Fieldset 2 : Description
        ('Détails de la réponse reçue de l\'api VOD', {
           'description': 'Le formulaire accepte les balises HTML. Utilisez-les à bon escient !',
           'fields': ( 'datareq',  'body', 'header', 'duree')
        }),
    )

admin.site.register(Pod, PodAdmin)
admin.site.register(Streamer, StreamerAdmin)
admin.site.register(Channel, ChannelAdmin)
admin.site.register(TypeMode)
admin.site.register(Mode, ModeAdmin)
admin.site.register(TypeCommande, TypeCommandeAdmin)
admin.site.register(Commande, CommandeAdmin)

##### API VOD
admin.site.register(Request, RequestAdmin)
admin.site.register(Response, ResponseAdmin)
######ponse)