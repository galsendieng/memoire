# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.utils import timezone

# Create your models here.
"""
def __unicode__(self):
    return u'{}'.format(self.id)
"""
""" PARTIE ABSTRACT """

class Request(models.Model):
    nom = models.CharField(max_length=255)
    link = models.URLField()
    datareq = models.TextField()
    header = models.CharField(max_length=255, default="{'Content-Type': 'application/xml'}")
    date = models.DateTimeField(auto_now=False,
                               verbose_name="Date de creation", default= timezone.now)

    def __str__(self):
        return self.nom

class Response(models.Model):
    nom = models.CharField(max_length=255)
    daterequest = models.DateTimeField(auto_now=False,
                               verbose_name="Date de test", default= timezone.now)
    link = models.URLField()
    datareq = models.TextField()
    status = models.CharField(max_length=10)
    reason = models.CharField(max_length=255)
    methode = models.CharField(max_length=100)
    body = models.TextField()
    header = models.CharField(max_length=255)
    duree = models.CharField(max_length=255)
    typetest = models.CharField(max_length=6, default='auto')
    #fichier = models.FileField(upload_to='uploads/', default='kkg.txt')

    def __str__(self):
        return self.nom


class ListeDeMode(models.Model):
    name = models.CharField(max_length=128)

    def __str__(self):
        return self.name

class Mode(models.Model):
    objet = models.ForeignKey(ListeDeMode)
    nameMode = models.CharField(max_length=100)
    staticAbr = models.CharField(max_length=100)
    fragment = models.IntegerField(default=2)
    manifestSuffix = models.CharField(max_length=10)
    deviceProfil = models.CharField(max_length=10)
    client = models.CharField(max_length=100, default='null')

    def __str__(self):
        return self.nameMode

class ListeDeCommande(models.Model):
    name = models.CharField(max_length=128)
    def __str__(self):
        return self.name

class CommandeStream(models.Model):
    objet = models.ForeignKey(ListeDeCommande)
    nameCommande = models.CharField(max_length=100)
    period = models.IntegerField(default=0)
    end = models.CharField(max_length=50, default='end')

    def __str__(self):
        return self.nameCommande


class NamePod(models.Model):
    name = models.CharField(max_length=100, unique=True)
    description = models.TextField(null=True, default='Description du Pod')
    created = models.DateTimeField(auto_now=False,
                               verbose_name="Date de creation", default= timezone.now)

    def __str__(self):
        return str(self.name)

class ListeDePod(models.Model):
    name = models.ForeignKey(NamePod, blank=True, null=True)
    link = models.CharField(max_length=100,default='strm.podX.manager.cdvr.orange.fr', unique=True)
    created = models.DateTimeField(auto_now=False,
                               verbose_name="Date de creation", default= timezone.now)
    def __str__(self):
        return str(self.name)

class NameChaine(models.Model):
    name = models.CharField(max_length=100)
    description = models.TextField(default='Description du Noeud')
    created = models.DateTimeField(auto_now=False,
                               verbose_name="Date de creation", default= timezone.now)

    def __str__(self):
        return str(self.name)

class Chaine(models.Model):
    objet = models.ForeignKey(ListeDePod)
    name = models.ForeignKey(NameChaine)

    def __str__(self):
        return str(self.name)

class NameNode(models.Model):
    name = models.CharField(max_length=100, unique=True)
    description = models.TextField(null=True, default='Description du Noeud')
    created = models.DateTimeField(auto_now=False,
                               verbose_name="Date de creation", default= timezone.now)

    def __str__(self):
        return str(self.name)

class Streamer(models.Model):
    objet = models.ForeignKey(ListeDePod, blank=True, null=True)
    node = models.ForeignKey(NameNode, blank=True, null=True)
    address = models.GenericIPAddressField()
    created = models.DateTimeField(auto_now=False,
                               verbose_name="Date de creation", default= timezone.now)

    def __str__(self):
        return str(self.address)

class Configuration(models.Model):

    nameConfig = models.CharField(max_length=100)
    intervalle = points = models.PositiveIntegerField(default=15)
    emailFrom = models.EmailField(default='adama.dieng@orange.com')
    smsFrom = models.CharField(default='+33768225617')
    protocole = models.CharField(max_length=100)
    port = models.PositiveIntegerField(default=5555)
    schemasmooth = models.FilePathField(max_length=100)
    timeout = models.PositiveIntegerField(default=15)
    nameOfLevelFile = models.CharField(max_length=100)
    folderOfLevelFile = models.CharField(max_length=100)
    created = models.DateTimeField(auto_now=False,
                               verbose_name="Date de creation",
                                   default= timezone.now)

    def __str__(self):
        return str(self.nameConfig)
