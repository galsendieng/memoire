# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from .models import *
# Register your models here.

class StreamerAdmin(admin.ModelAdmin):
    #list_display = ('titre', 'headline', 'pub_date', 'apercu_contenu')
    list_display = ('address', 'created')
    list_filter = ('address', 'created')
    date_hierarchy = 'created'
    ordering = ('created',)
    search_fields = ('address', 'created')


class ChannelAdmin(admin.ModelAdmin):
    #list_display = ('titre', 'headline', 'pub_date', 'apercu_contenu')
    list_display = ('nom', 'description', 'created','pods')
    list_filter = ('nom', 'description', 'created','pods')
    date_hierarchy = 'created'
    ordering = ('created',)
    search_fields = ('nom', 'pods')

    # Configuration du formulaire d'édition
    fieldsets = (
        # Fieldset 1 : meta-info (titre, auteur…)
       ('Général', {
            'classes': ['collapse', ],
            'fields': ('nom',  'created','pods')
        }),
        # Fieldset 2 : contenu de l'article
        ('Contenu de la Chaine', {
           'description': 'Le formulaire accepte les balises HTML. Utilisez-les à bon escient !',
           'fields': ('description',)
        }),
    )

class CommandeAdmin(admin.ModelAdmin):
    #list_display = ('titre', 'headline', 'pub_date', 'apercu_contenu')
    list_display = ('nameCommande', 'description', 'created')
    list_filter = ('nameCommande', 'description', 'created')
    date_hierarchy = 'created'
    ordering = ('created',)
    search_fields = ('nameCommande',)

    # Configuration du formulaire d'édition
    fieldsets = (
        # Fieldset 1 : meta-info (titre, auteur…)
       ('Général', {
            'classes': ['collapse', ],
            'fields': ('nameCommande',  'created')
        }),
        # Fieldset 2 : contenu de l'article
        ('Détails', {
           'description': 'Le formulaire accepte les balises HTML. Utilisez-les à bon escient !',
           'fields': ('description',)
        }),
    )

class PodAdmin(admin.ModelAdmin):
    #list_display = ('titre', 'headline', 'pub_date', 'apercu_contenu')
    list_display = ('name', 'link', 'description', 'created')
    list_filter = ('name', 'link', 'created')
    date_hierarchy = 'created'
    ordering = ('created',)
    search_fields = ('name',)

    # Configuration du formulaire d'édition
    fieldsets = (
        # Fieldset 1 : meta-info (titre, auteur…)
       ('Infos principales du Pod', {
            'classes': ['collapse', ],
            'fields': ('name', 'link', 'created')
        }),
        # Fieldset 2 : contenu de l'article
        ('Description du Pod', {
           'description': 'Le formulaire accepte les balises HTML. Utilisez-les à bon escient !',
           'fields': ('description',)
        }),
    )

class TypeCommandeAdmin(admin.ModelAdmin):
    #list_display = ('titre', 'headline', 'pub_date', 'apercu_contenu')
    list_display = ('name', 'period', 'start', 'end', 'description', 'created')
    list_filter = ('name', 'period', 'created')
    date_hierarchy = 'created'
    ordering = ('created',)
    search_fields = ('name',)

    # Configuration du formulaire d'édition
    fieldsets = (
        # Fieldset 1 : meta-info (titre, auteur…)
       ('Infos principales de la Commande', {
            'classes': ['collapse', ],
            'fields': ('name', 'period', 'start', 'end',  'created')
        }),
        # Fieldset 2 : contenu de l'article
        ('Description du Pod', {
           'description': 'Le formulaire accepte les balises HTML. Utilisez-les à bon escient !',
           'fields': ('commande', 'description',)
        }),
    )

class RequestAdmin(admin.ModelAdmin):
    #list_display = ('titre', 'headline', 'pub_date', 'apercu_contenu')
    list_display = ('nom', 'link', 'date', 'datareq', 'header')
    list_filter = ('nom', 'link', 'date', 'header')
    date_hierarchy = 'date'
    ordering = ('nom',)
    search_fields = ('nom', 'link','date', )

    # Configuration du formulaire d'édition
    fieldsets = (
        # Fieldset 1 : meta-info (titre, auteur…)
       ('Général', {
            'classes': ['collapse', ],
            'fields': ('nom', 'link', 'date')
        }),
        # Fieldset 2 : contenu de l'article
        ('Description de la requête', {
           'description': 'Le formulaire accepte les balises HTML. Utilisez-les à bon escient !',
           'fields': ('datareq', 'header')
        }),
    )

class ResponseAdmin(admin.ModelAdmin):
    list_display = ('nom', 'daterequest', 'link', 'datareq', 'status', 'body', 'typetest')
    readonly_fields=('typetest', )
    list_filter = ('nom', 'daterequest', 'link', 'datareq', 'status', 'reason')
    date_hierarchy = 'daterequest'
    ordering = ('daterequest', 'nom')
    search_fields = ('nom', 'daterequest', 'link', 'status', 'reason',)

    # Configuration du formulaire d'édition
    fieldsets = (
       ('Général', {
            'classes': ['collapse', ],
            'fields': ('nom', 'daterequest', 'link', 'status', 'reason', 'methode', 'typetest' )
        }),
        # Fieldset 2 : Description
        ('Détails de la réponse reçue de l\'api VOD', {
           'description': 'Le formulaire accepte les balises HTML. Utilisez-les à bon escient !',
           'fields': ( 'datareq',  'body', 'header', 'duree')
        }),
    )




""" Modes """
class ModeAdminInline(admin.TabularInline):
    model = Mode

class ListeModeAdmin(admin.ModelAdmin):
    inlines = (ModeAdminInline, )

""" Commandes """
class CommandeAdminInline(admin.TabularInline):
    model = CommandeStream

class ListeCommandeAdmin(admin.ModelAdmin):
    inlines = (CommandeAdminInline, )

""" Pods """
class ChaineAdminInline(admin.TabularInline):
    model = Chaine

class StreamerAdminInline(admin.TabularInline):
    model = Streamer

class ListePodAdmin(admin.ModelAdmin):
    inlines = (ChaineAdminInline, StreamerAdminInline)
    #val = Chaine
    #print(val.name)
    list_display = ('name', 'link', 'created' )
    list_filter = ('name', 'link', 'created')
    date_hierarchy = 'created'
    ordering = ('created',)
    search_fields = ('name',)

""" registre """
admin.site.register(NameChaine)
admin.site.register(NameNode)
admin.site.register(NamePod)
admin.site.register(ListeDeMode, ListeModeAdmin)
admin.site.register(ListeDeCommande, ListeCommandeAdmin)
admin.site.register(ListeDePod, ListePodAdmin)

#admin.site.register(StreamerAdmin)
#admin.site.register(Mode, ModeAdmin)
##### API VOD
admin.site.register(Request, RequestAdmin)
admin.site.register(Response, ResponseAdmin)
######
admin.site.register(Configuration)
