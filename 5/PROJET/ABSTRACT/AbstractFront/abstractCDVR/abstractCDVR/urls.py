#-*- coding: utf-8 -*-
from abstract import views
from django.conf.urls import include, url
from django.contrib import admin
from django.utils.translation import ugettext_lazy as _

urlpatterns = [
    #url(r'^admin/', admin.site.urls),
    url(r'^$', views.home),
    url(r'^admin/', include(admin.site.urls)),
]
# Change admin site title
admin.site.site_header = _("ABSTRACT")
admin.site.site_title = _("ABSTRACT")
