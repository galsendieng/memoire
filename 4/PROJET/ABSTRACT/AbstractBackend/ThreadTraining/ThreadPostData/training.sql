-- phpMyAdmin SQL Dump
-- version 4.2.12deb2+deb8u1build0.15.04.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Mer 17 Mai 2017 à 09:52
-- Version du serveur :  5.6.28-0ubuntu0.15.04.1
-- Version de PHP :  5.6.4-4ubuntu6.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `training`
--

-- --------------------------------------------------------

--
-- Structure de la table `requests`
--

CREATE TABLE IF NOT EXISTS `requests` (
`id` int(11) NOT NULL,
  `nom` varchar(60) NOT NULL,
  `link` varchar(300) NOT NULL,
  `datareq` mediumtext NOT NULL,
  `header` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `requests`
--

INSERT INTO `requests` (`id`, `nom`, `link`, `datareq`, `header`) VALUES
(1, 'fetch file with API', 'http://192.168.134.3:5929/v2/fetch_file', '<?xml version="1.0" encoding="UTF-8"?>\r\n<fetch_file>\r\n<fetch_from>ftp://vspp:W3lc0m3TVSt1r@192.168.134.119/cdvr/masters/MASTER_LASSAUT.ts</fetch_from>\r\n<fetch_to>/root/masters.ts</fetch_to>\r\n<initiator>Manu</initiator>\r\n</fetch_file>', '{''Content-Type'': ''application/xml''}'),
(2, 'Transcoding Files', 'http://192.168.134.3:5929/v2/transcode_package', '<?xml version="1.0" encoding="UTF-8"?>\r\n<transcode_package>\r\n<input_file>/root/masters_lassaut.ts</input_file>\r\n<output_path>/vms_transcoded/masters_lassaut_zzRomania4/</output_path>\r\n<trans_profile>zzRomania4</trans_profile>\r\n<initiator>Manu</initiator>\r\n</transcode_package>', '{''Content-Type'': ''application/xml''}'),
(3, 'Create ABR Asset', 'http://192.168.134.3:5929/v2/createABRasset', '<?xml version="1.0" encoding="UTF-8"?>\r\n<createABRasset>\r\n<id>THEDARKKNIG_NoSubtitle</id>\r\n<name>THEDARKKNIG_NoSubtitle</name>\r\n<initiator>OLPS</initiator>\r\n<input_directory>/mnt/ftp_vol/vms_transcoded/THEDARKKNIG_zzRomania4/</input_directory>\r\n</createABRasset>', '{''Content-Type'': ''application/xml''}'),
(4, 'Ingestion of SRT', 'http://192.168.134.3:5929/v2/createABRasset', '<?xml version="1.0" encoding="UTF-8"?>\r\n<createABRasset>\r\n<id>miller_3_sub</id>\r\n<name>miller_3_sub</name>\r\n<initiator>OLPS</initiator>\r\n<input_directory>/mnt/ftp_vol/transcoded/miller</input_directory>\r\n</createABRasset>', '{''Content-Type'': ''application/xml''}');

-- --------------------------------------------------------

--
-- Structure de la table `responses`
--

CREATE TABLE IF NOT EXISTS `responses` (
`id` int(11) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `daterequest` varchar(100) NOT NULL,
  `link` varchar(100) NOT NULL,
  `datareq` varchar(9999) NOT NULL,
  `status` varchar(50) NOT NULL,
  `reason` varchar(50) NOT NULL,
  `methode` varchar(50) NOT NULL,
  `body` varchar(9999) NOT NULL,
  `headers` varchar(9999) NOT NULL,
  `duree` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4655 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `resultats`
--

CREATE TABLE IF NOT EXISTS `resultats` (
`id` int(11) NOT NULL,
  `daterequete` datetime NOT NULL,
  `codereponse` varchar(50) NOT NULL,
  `message` mediumtext NOT NULL,
  `contenu` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
`id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `user`
--

INSERT INTO `user` (`id`, `name`, `email`, `password`) VALUES
(1, 'adama', 'adama@test.fr', 'test');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `requests`
--
ALTER TABLE `requests`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `responses`
--
ALTER TABLE `responses`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `resultats`
--
ALTER TABLE `resultats`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `requests`
--
ALTER TABLE `requests`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `responses`
--
ALTER TABLE `responses`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4655;
--
-- AUTO_INCREMENT pour la table `resultats`
--
ALTER TABLE `resultats`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
