#-*- coding: utf-8 -*-#
import os
from django.core.exceptions import ValidationError


def validate_file_extension(value):
    ext = os.path.splitext(value.name)[1]  # [0] returns path+filename
    valid_extensions = ['.xsd']
    if not ext.lower() in valid_extensions:
        raise ValidationError(u'Unsupported file extension.')

def validate_file_xsd(value):
    try:
        ext = os.path.splitext(value.name)[1]  # [0] returns path+filename
        valid_extensions = ['.xsd']
        if not ext.lower() in valid_extensions:
            raise ValidationError(u'Unsupported file extension.')
    except Exception as x:
        print(x)

def generateConfig():
    try:
        print("Génération d'une configuration ...")
        #print(DEBUG)
    except Exception as x:
        print(x)

